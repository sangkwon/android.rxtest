package rxtest.realmove.egloos.com.rxtest;

import com.hwangjr.rxbus.Bus;

/**
 * Created by sangkwon on 2016. 6. 24..
 */
public final class RxBus {

	private static Bus instance = new Bus();

	public static synchronized Bus get() {
		return instance;
	}

}
