package rxtest.realmove.egloos.com.rxtest;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.widget.TextView;

import com.jakewharton.rxbinding.view.RxView;

import rx.Observable;
import rx.Subscriber;
import rx.functions.Action1;
import rx.functions.Func1;
import rx.schedulers.Schedulers;

public class MainActivity extends AppCompatActivity {

	private static final String TAG = MainActivity.class.getSimpleName();

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);

		//				rxTest1();
		//		rxTest2();
		//		rxText3();
		rxText4();
	}

	private void rxText4() {
		Observable
			.range(1, 10)
			.map(integer -> integer * 10)
			.subscribeOn(Schedulers.io())
			.subscribe(value -> {
				TextView t = (TextView) findViewById(android.R.id.text1);
				t.setText("count:" + count);
			});
	}

	private int count = 0;

	private void rxText3() {
		Observable ob1 = RxView.clicks(findViewById(android.R.id.button1));
		Observable ob2 = RxView.clicks(findViewById(android.R.id.text1));
		Observable
			.merge(ob1, ob2)
			.map(event -> count++)
			.subscribe(value -> {
				TextView t = (TextView) findViewById(android.R.id.text1);
				t.setText("count:" + count);
			});

	}

	private void rxTest2() {
		Observable
			.range(1, 10)
			.map(integer -> integer * 10)
			.map(integer -> "***" + integer + "***")
			.subscribeOn(Schedulers.computation())
			.observeOn(Schedulers.io())
			.subscribe(System.out::println);
	}

	private void rxTest1() {
		String[] strs = { "a", "b", "c" };
		Observable<String> simpleObservable = Observable.from(strs);

		simpleObservable.subscribe(new Subscriber<String>() {
			@Override
			public void onCompleted() {
				Log.d(TAG, "complete!");
			}

			@Override
			public void onError(Throwable e) {
				Log.d(TAG, "onError!" + e);
			}

			@Override
			public void onNext(String s) {
				Log.d(TAG, "onNext!" + s);
			}
		});
	}
}
